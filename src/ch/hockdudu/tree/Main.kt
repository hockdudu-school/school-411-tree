package ch.hockdudu.tree

fun main(args : Array<String>) {
    val values = intArrayOf( 100, 50, 210, 210, 30, 70, 150, 300, 60, 88, 250, 210)

    val rootNode = Node(values.first())

    for (i in 1..values.lastIndex) {
        rootNode += values[i]
    }

    rootNode.print()

    rootNode -= 100
    rootNode -= 60
    rootNode -= 210
    rootNode -= 210
    rootNode -= 210

    rootNode.print()

    rootNode.preOrder()
    rootNode.inOrder()
    rootNode.outOrder()
    rootNode.postOrder()
    rootNode.levelOrder()
}
