package ch.hockdudu.tree

fun Node.preOrder() {
    val list = mutableListOf<Node>()

    fun traverse(child: Node?) {
        if (child == null) return

        list += child

        traverse(child.leftChild)
        traverse(child.rightChild)
    }

    traverse(this)

    println(list)
}

fun Node.inOrder() {
    val list = mutableListOf<Node>()

    fun traverse(child: Node?) {
        if (child == null) return

        traverse(child.leftChild)
        list += child
        traverse(child.rightChild)
    }

    traverse(this)

    println(list)
}

fun Node.outOrder() {
    val list = mutableListOf<Node>()

    fun traverse(child: Node?) {
        if (child == null) return

        traverse(child.rightChild)
        list += child
        traverse(child.leftChild)
    }

    traverse(this)

    println(list)
}


fun Node.postOrder() {
    val list = mutableListOf<Node>()

    fun traverse(child: Node?) {
        if (child == null) return

        traverse(child.leftChild)
        traverse(child.rightChild)

        list += child
    }

    traverse(this)

    println(list)
}

fun Node.levelOrder() {
    val list = mutableListOf<Node>()

    fun traverse(children: List<Node>) {

        if (children.isEmpty()) return

        list += children

        val nextLevelChildren = mutableListOf<Node>()

        children.forEach {
            it.leftChild?.let { child ->
                nextLevelChildren += child
            }

            it.rightChild?.let { child ->
                nextLevelChildren += child
            }
        }

        traverse(nextLevelChildren)
    }

    traverse(listOf(this))

    println(list)
}