package ch.hockdudu.tree

import java.lang.Exception
import kotlin.math.max

class Node(var value: Int) {

    var leftChild: Node? = null
        private set

    var rightChild: Node? = null
        private set

    var parent: Node? = null
        private set

    val size: Int
        get() {
            var length = 1

            leftChild?.let {
                length += it.size
            }

            rightChild?.let {
                length += it.size
            }

            return length
        }

    val depth: Int
        get() {
            var depth = 1

            leftChild?.let {
                depth += it.depth
            }

            return 1 + max(leftChild?.depth ?: 0, rightChild?.depth ?: 0)
        }

    fun insert(node: Node) {
        if (node < this) {
            addLeft(node)
        } else {
            addRight(node)
        }
    }

    fun getNodeWithValue(value: Int): Node? {
        return if (this.value == value) {
            this
        } else {
            if (value < this.value)
                leftChild?.getNodeWithValue(value)
            else
                rightChild?.getNodeWithValue(value)
        }
    }

    fun delete(node: Node) {
        when {
            node > this -> rightChild?.delete(node)
            node < this -> leftChild?.delete(node)

            // If we must be deleted
            else -> {
                if (parent == null) {
                    when {
                        leftChild != null -> {
                            val leftChildGreatest = leftChild!!.getGreatest()
                            this.value = leftChildGreatest.value
                            leftChild!!.delete(leftChildGreatest)
                        }

                        rightChild != null -> {
                            val rightChildLowest = rightChild!!.getLowest()
                            this.value = rightChildLowest.value
                            rightChild!!.delete(rightChildLowest)
                        }

                        // No parent, no children, you must be deleted, what can you possibly do?!
                        else -> throw Exception("Cannot delete lone node")
                    }
                } else {
                    when {
                        // Both are null, we can be deleted without problems
                        leftChild == null && rightChild == null -> setNodeOnCurrentPosition(null)

                        // Right child is not null
                        leftChild == null -> {
                            setNodeOnCurrentPosition(rightChild)
                        }

                        // Left child is not null
                        rightChild == null -> {
                            setNodeOnCurrentPosition(leftChild)
                        }

                        // Both children aren't null
                        else -> {
                            val leftGreatest = leftChild!!.getGreatest()
                            this.value = leftGreatest.value
                            leftGreatest.setNodeOnCurrentPosition(leftGreatest.leftChild)
                        }
                    }
                }
            }
        }
    }

    fun getLowest(): Node {
        return leftChild?.getLowest() ?: this
    }

    fun getGreatest(): Node {
        return rightChild?.getGreatest() ?: this
    }

    private fun addLeft(node: Node) {
        if (leftChild == null) {
            leftChild = node
            node.parent = this
        } else {
            leftChild!!.insert(node)
        }
    }

    private fun addRight(node: Node) {
        if (rightChild == null) {
            rightChild = node
            node.parent = this
        } else {
            rightChild!!.insert(node)
        }
    }

    /**
     * Sets either the left or right child of parent, depending where we are
     */
    private fun setNodeOnCurrentPosition(node: Node?) {
        parent?.let {
            node?.parent = it
            if (it.leftChild == this) {
                it.leftChild = node
            } else {
                it.rightChild = node
            }
        }

    }

    operator fun compareTo(node: Node): Int {
        return compareValues(value, node.value)
    }

    operator fun plusAssign(node: Node) {
        insert(node)
    }

    operator fun plusAssign(value: Int) {
        insert(Node(value))
    }

    operator fun minusAssign(node: Node) {
        delete(node)
    }

    operator fun minusAssign(value: Int) {
        getNodeWithValue(value)?.let {
            delete(it)
        }
    }

    operator fun contains(value: Int): Boolean = getNodeWithValue(value) != null

    override fun toString(): String {
        return value.toString()
    }
}