package ch.hockdudu.tree

fun Node.print(invertDirection: Boolean = false) {
    println(print(this, "", true, invertDirection))
}

private fun print(node: Node, prefix: String, isBottomHalf: Boolean, invertDirection: Boolean): String {
    var string = ""

    (if (invertDirection) node.rightChild else node.leftChild)?.let {
        string += print(it, prefix + (if (isBottomHalf) "│   " else "    "), false, invertDirection)
    }

    string += prefix + (if (isBottomHalf) "└── " else "┌── ") + node.value + "\n"

    (if (invertDirection) node.leftChild else node.rightChild)?.let {
        string += print(it, prefix + (if (isBottomHalf) "    " else "│   "), true, invertDirection)
    }

    return string
}
